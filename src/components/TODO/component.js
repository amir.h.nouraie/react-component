import React, { Component } from 'react';
import './component.css';

class TODO extends Component {
    constructor(props) {
        super(props);

        this.state = {
            item_input: '',
            items: []
        };

        this.update = this.update.bind(this);
        this.add = this.add.bind(this);
    }

    update(event) {
        this.setState({
            item_input: event.target.value
        });
    }

    add() {
        this.setState(prev => {
            return {
                item_input: '',
                items: prev.items.concat(prev.item_input)
            };
        });
    }

    handleKeyPress = event => {
        if (event.key === 'Enter') {
            this.add();
        }
    }

    render() {
        return (
            <div className="holder">
                <h4>ToDo list</h4>
                <ul className="list">
                    {
                        this.state.items.map(
                            (item, i) => (
                                <li>{item}</li>
                            )
                        )
                    }
                </ul>
                <input
                    type="text"
                    value={this.state.item_input}
                    onChange={this.update}
                    onKeyPress={this.handleKeyPress} />
                <button
                    type="button"
                    onClick={this.add}>add item</button>
            </div>
        );
    }
}

export default TODO;